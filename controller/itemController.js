const fervis = require('../models/fervis')
const paginate = require('express-paginate')
//handling photos upload
const multer = require('multer');
const path = require('path')
const items = {};

//get all lists of ninjas from database

items.getall = function(req,res, next) {
	fervis.FervisItem.find({}).populate({
		path : 'user_id',
		select : 'username password created_at'
	}).then(function(data){
		res.send(data)
	}

	);
};	

//get single item from database
items.getsingle = function(req,res, next) {
	fervis.FervisItem.findOne({_id : req.params.id}).then(function(data){
			fervis.FervisImage.find({ item_id : req.params.id},
				 '-fieldname -mimetype -encoding -item_id -user_id')
			.then(function(dataimage){
				var newData = data.toObject()
				newData.img = dataimage
				res.render('details', {data : newData});
			})
		}).catch(next);
	};

//Add new single item to database
items.post = function (req,res, next) {

	var dataToInsert = req.body	
	dataToInsert.user_id = req.user._id
	console.log(dataToInsert)

		fervis.FervisItem.create(dataToInsert).then(function(data){
		res.redirect('/item')
	}).catch(next);
};

// upload
// Set Storage Engine

// Init Upload
items.getphoto = function(req, res){
	res.render('upload', {
		message : req.flash('msg')
	})
	
}

items.uploadphoto = function (req, res, next){
	
	const storage = multer.diskStorage({
 		destination: './public/uploads/',
  		filename: function(req, file, callback){
    		callback(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  		}
	});
	const upload = multer({
  		storage: storage,
  		limits:{fileSize: 1000000},
  		fileFilter: function(req, file, cb){
    		checkFileType(file, cb);
  		}
	}).single('img');
	
	
	upload(req, res, function (err) {
    	console.log(req.file)
      if (err){ // this error came from CheckFileType function 
      		console.log(err)
      		res.render('upload', { msg: req.flash('msg', err) }, function (){
      			res.redirect('/upload') } );
      } else {
      if(req.file == undefined){
          res.render('upload', { msg : req.flash('msg', 'No File Selected!')}, function (){
          	res.redirect('/upload')
          });
      } else {
          res.render('upload', {msg: req.flash('msg','File Uploaded!')}, function (){
          	const dataToInsert = req.body	
				dataToInsert.user_id = req.user._id
				dataToInsert.fieldname = req.file.fieldname
				dataToInsert.originalname = req.file.originalname
				dataToInsert.encoding = req.file.encoding
				dataToInsert.mimetype = req.file.mimetype
				dataToInsert.destination = req.file.destination
				dataToInsert.filename = req.file.filename
				dataToInsert.path = req.file.path
				dataToInsert.size = req.file.size
				console.log(dataToInsert)

          	fervis.FervisImage.create(dataToInsert).then(function(data){
          		res.redirect('/upload')
				console.log(data)
				}).catch(next);
          });
        }
    }  
   });
	
	// Check File Type
	function checkFileType(file, cb){
  	// Allowed ext
  	const filetypes = /jpeg|jpg|png|gif/;
  	// Check ext
  	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  	// Check mime
  	const mimetype = filetypes.test(file.mimetype);

  	if(mimetype && extname){
    	return cb(null,true);
  	} else {
    	cb('Invalid Images Format');
  	}
	}	
};	 	 
//Delete single item
items.delete = function (req,res, next) {
	fervis.FervisItem.findByIdAndRemove({_id : req.params.id }).then(function(data){
		
		if (data != null && data !== undefined){
			var sts = data.toJSON();
			sts.status = "Deleted";
			//console.log(typeof sts);
			res.send(sts['status']);
		} else {
			res.send('This file has been deleted')
		}	
	}).catch(next);
};

//replace single item from database
items.put =  function(req,res, next) {
	fervis.FervisItem.findByIdAndUpdate({_id : req.params.id}, req.body).then(function(){
		fervis.FervisItem.findOne({_id : req.params.id}).then(function(data){
			res.send(data);
		});
	});
};

items.pagination = function( req, res, next){
		console.log(req.user._id)
		fervis.FervisItem.paginate({"user_id" : req.user._id}, { page: req.query.page,
			 limit: req.query.limit }, function(err, items){

			if (err) return next(err);
		    res.format({
		      html: function() {
		        res.render('item', {
		          items: items.docs,
		          pageCount: items.pages,
		          itemCount: items.limit,
		          pages: paginate.getArrayPages(req)(3, items.pages, req.query.page)
		        });
		      },
		      json: function() {
		        // inspired by Stripe's API response for list objects
		        res.json({
		          object: 'list',
		          has_more: paginate.hasNextPages(req)(items.pages),
		          data: items.docs
		        });
		      }
				});    
		});
};
	
module.exports = items;